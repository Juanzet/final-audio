﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plat03 : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        AkSoundEngine.PostEvent("Plat03", gameObject);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        //AkSoundEngine.PostEvent("", gameObject);
    }
}
