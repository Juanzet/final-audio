﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plat01 : MonoBehaviour
{

    private void OnCollisionEnter2D(Collision2D collision)
    {
        AkSoundEngine.PostEvent("Plat01", gameObject);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        //AkSoundEngine.PostEvent("", gameObject);
    }
}
