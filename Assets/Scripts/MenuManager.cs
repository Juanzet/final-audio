﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
   
    public void StartGame()
    {
        SceneManager.LoadScene("Scene01GP");
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
