﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float _horizontal;
    private bool isFacingRight = true;
    private GameObject currentTeleporter;

    [SerializeField] private float _speed;
    [SerializeField] private float _jumpingPower;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform _groundCheck;
    [SerializeField] private LayerMask groundLayer;

    void Update()
    {
        _horizontal = Input.GetAxisRaw("Horizontal");
        if (Input.GetKey(KeyCode.W) && IsGrounded())
        {
            rb.velocity = new Vector2(rb.velocity.x, _jumpingPower);
        }
        if (Input.GetKey(KeyCode.W) && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }

        if (Input.GetButtonDown("Jump")&& IsGrounded())
        {
            AkSoundEngine.PostEvent("Jump", gameObject);
            rb.velocity = new Vector2(rb.velocity.x, _jumpingPower);
        }
        if (Input.GetButtonUp("Jump") && rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
        }

        TeleportPlayer();
        Flip();
    }

    void FixedUpdate()
    {
        rb.velocity = new Vector2(_horizontal * _speed, rb.velocity.y);
    }

    bool IsGrounded()
    {
        return Physics2D.OverlapCircle(_groundCheck.position, 0.2f, groundLayer);
    }

    void Flip()
    {
        if(isFacingRight && _horizontal < 0f || !isFacingRight && _horizontal > 0f)
        {
            isFacingRight = !isFacingRight;
            Vector3 localScale = transform.localScale;
            localScale.x *= -1f;
            transform.localScale = localScale;
        }
    }

    void TeleportPlayer()
    {
        if(Input.GetKeyDown(KeyCode.E)) 
        {
            if(currentTeleporter != null)
            {
                AkSoundEngine.PostEvent("Teleporter", gameObject);
                transform.position = currentTeleporter.GetComponent<Tp>().GetDestination().position;
            }

        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Teleport"))
        {
            currentTeleporter = collision.gameObject;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == currentTeleporter)
        {
            currentTeleporter = null;
        }
    }
}
